'use strict';

angular.module('pointofsale')
    .directive('header', function() {
        return {
            restrict: 'AE',
            templateUrl: 'templates/directives/header.html',
            replace: true
        };
    });
