'use strict';

angular.module('pointofsale')
    .factory('SupplierFactory', function($http, API_URL) {
        return {
            getAllSupplier: function() {
                return $http({
                    url: API_URL + '/supplier',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getSupplier: function(id) {
                return $http({
                    url: API_URL + '/supplier/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteSupplier: function(id) {
                return $http({
                    url: API_URL + '/supplier/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveSupplier: function(data) {
                return $http({
                    url: API_URL + '/supplier',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateSupplier: function(id, data) {
                return $http({
                    url: API_URL + '/supplier/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
