'use strict';

angular
    .module('pointofsale', ['ngResource', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'ngAnimate', 'toastr', 'ngDialog', 'ngTable', 'ngCookies', 'oc.lazyLoad', 'angular-loading-bar'])
    .constant('API_URL', 'http://localhost:3000/api/1.0')
    .config(function($httpProvider, $ocLazyLoadProvider) {
        $httpProvider.interceptors.push('authInterceptor');
        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
        });
    })
    .factory('authInterceptor', function($q, $window, $location) {
        return {
            // Add authorization token to headers
            request: function(config) {
                config.headers = config.headers || {};
                if ($window.localStorage.userToken) {
                    config.headers.Authorization = 'Bearer ' + $window.localStorage.userToken;
                }
                return config;
            },

            // Intercept 401s and redirect you to login
            responseError: function(response) {
                if (response.status === 401) {

                    $window.localStorage.userToken = '';
                    $window.localStorage.user = '';

                    $location.path('/login');

                    return $q.reject(response);
                } else if (response.status === 500) {
                    // $location.path('/login');
                    return $q.reject(response);
                } else if (response.status === 0) {
                    // $location.path('/login');
                    return $q.reject(response);
                } else {
                    return $q.reject(response);
                }
            }
        };
    });
