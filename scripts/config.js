'use strict';

angular.module('pointofsale')
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider
        // =========== AUTHENTICATION =========== //
            .state('main', {
            url: '/main',
            templateUrl: 'templates/main.html',
        })
            .state('addProd', {
            url: '/addProd',
            templateUrl: 'templates/addProd.html',
        });

        // =========== DASHBOARD =========== //
        

        $urlRouterProvider.otherwise('/main');
    });
